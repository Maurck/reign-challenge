# Reign Challenge

This is a small API to access data from Hacker News ( https://hn.algolia.com/api/v1/search_by_date?query=nodejs ) stored in a separate mongoDB database, using NestJS.

## Requisites

- Docker

## Steps to run the application

- Create a `.env` file at the root of the project using the `.env.example` file present in the project

### .env fields:
| Field | Description |
| -- | -- |
| MONGODB_URI | URI of the mongodb database (default: mongodb://mongo:27017/challenge)
| SECRET_KEY | Secret key used to generate Json Web tokens |
| ADMIN_USERNAME | Username used in the login route (`/auth/login`) to get a Json Web token for authentication |
| ADMIN_PASSWORD | Password used in the login route (`/auth/login`) to get a Json Web token for authentication |

- Populate .env file
- Open a CMD or SHELL in the root directory of the project
- Execute command: `docker-compose up dev mongo -d`
- Open your favorite browser and search `localhost:5000/` to see something special
- The application is running!

## Swagger
After you run the application, the swagger page is located in `localhost:500/api/docs`

## Steps to use the API
The application uses a JWT based authentication to access the majority of the routes, any client must provide a valid JWT in the authorization header of each request or it will return a 401 error (`Unauthorized`)
- Run the application
### Authorization

- Make a GET request to `localhost:5000/auth/login` with query parameters:
| Query field | Description |
| -- | -- |
| username | `ADMIN_USERNAME` field of .env file |
| password | `ADMIN_PASSWORD` field of .env file |

The request will return a valid JWT, you will have to use this JWT in every authorization header of any request to the API in the form of: `authorization:  Bearer {JWT}`

### Populating the database

- Make a POST request to `localhost:5000/db` using the JWT in the Authorization header.

Now the database is populated!

For a complete description of every route of the API, use the swagger interface located at `localhost:5000/api/docs`

## Steps to use the API using Swagger

- Run the application
- Open your favorite browser at: `localhost:5000/api/docs`
You will see an interface like this:

![swagger-interface](https://i.ibb.co/xS1Nps0/swagger-interface.png)

### Authorization

- Go to the login route and click it:

![swagger-auth-login](https://i.ibb.co/MRCHjfs/swagger-auth-login.jpg)

- Click in `Try it out`:

![swagger-auth-login-try-out](https://i.ibb.co/9nrQpQz/swagger-auth-login-try-out.jpg)

- Populate username and password fields:

| Query field | Description |
| -- | -- |
| username | `ADMIN_USERNAME` field of .env file |
| password | `ADMIN_PASSWORD` field of .env file |

![swagger-auth-login-fields](https://i.ibb.co/WPF1m5C/swagger-auth-login-fields.jpg)

- Click in `Execute`

- It will give you a valid JWT as response body

![swagger-auth-login-response](https://i.ibb.co/LrrmJ8y/swagger-auth-login-response.jpg)

- Copy the JWT without quotes

- Go to `Authorize` and click it

![swagger-auth-login-authorize](https://i.ibb.co/z46wqPP/swagger-auth-login-authorize.jpg)

- Paste JWT in `Value` field
![swagger-auth-login-authorize](https://i.ibb.co/pyqp8GK/swagger-auth-login-authorize-jwt.jpg)

- Click in `Authorize` button

Now you are authorized to use the API routes using Swagger!

PD: You must repeat the process every time you reload the swagger page.

### Populating the database

- Go to `db` and click it

![swagger-db](https://i.ibb.co/j6gm4n1/swagger-db.jpg)

- Click in `Try it out`

![swagger-db-try-it-out](https://i.ibb.co/2PbbmzS/swagger-db-try-it-out.jpg)

- Click in `Execute`

![swagger-db-execute](https://i.ibb.co/VDg9xpQ/swagger-db-execute.jpg)

Now the database is populated with the external API's data!

From here you can use the routes using the Swagger interface :)

## Generate code documentation (Compodoc)

- Open a CMD or SHELL in the root directory of the project
- Run the application
- Execute command: `docker exec -it reign-challenge_api_dev /bin/sh`
It will open a bash in the docker container of the application

- Execute command in bash: `npx @compodoc/compodoc -p tsconfig.json -s`
- Open your favorite browser and search `http://127.0.0.1:8080`
You will be able to see the full documentation of the project

## Testing

- Open a CMD or SHELL in the root directory of the project
- Run the application
- Execute command: `docker exec -it reign-challenge_api_dev /bin/sh`
It will open a bash in the docker container of the application

- Execute command in bash: `npx jest --coverage`
It will show the tests coverage and tests status
