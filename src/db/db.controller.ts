import { Controller, HttpStatus, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DbService } from './db.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

/** Database Controller */
@ApiTags('db')
@Controller('db')
export class DbController {

  /** @ignore */
  constructor(private readonly dbService: DbService) {}

  /**
   * POST Method to populate database using external API
   * @param res Response object
   * @returns Promise of type Response
   */
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Sucessfully populated database',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not populate database',
  })
  @ApiOperation({ summary: 'Populate news database' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Post()
  async populateDatabase(@Res() res: Response) {
    try {
      await this.dbService.loadNewsToDatabase();
      return res.status(HttpStatus.OK).json({
        msg: 'Database populated successfully',
      });
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        msg: 'There was an error populating the database',
      });
    }
  }
}
