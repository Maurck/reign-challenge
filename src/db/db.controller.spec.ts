import { Test, TestingModule } from '@nestjs/testing';
import { DbController } from './db.controller';
import { DbService } from './db.service';
import { Response } from 'express';

describe('DbController', () => {
  let controller: DbController;

  const mockDbService = {}

  let mockResponse: Partial<Response>;
    
  let resultJson = {};
  let resultStatus = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DbController],
      providers: [DbService]
    }).overrideProvider(DbService).useValue(mockDbService).compile();

    controller = module.get<DbController>(DbController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should populate database', () => {

		mockResponse = {};

		resultJson = {};
		resultStatus = {};

    mockResponse.status = jest.fn().mockImplementation((result) => {
			resultStatus = result;
			return mockResponse;
		});
		mockResponse.json = jest.fn().mockImplementation((result) => {
			resultJson = result;
			return mockResponse;
		});
    
    controller.populateDatabase(mockResponse as Response);

    expect(mockResponse).toEqual({
      status: expect.anything(),
      json: expect.anything(),
    })
  })
});
