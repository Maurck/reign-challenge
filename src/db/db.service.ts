import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import fetch from 'node-fetch';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { News } from '../classes/news.class'
import { INews } from '../interface/news.interface';

/** Database Service */
@Injectable()
export class DbService {

    /** @ignore */
    constructor(@InjectModel('News') private readonly newsModel: Model<INews>) {}
    
    /**
     * Method that returns the news from the external API
     * @returns Promise of type Array of type News
     */
    async getNewsFromAPI(): Promise<[]> {
        try {
          const response = await fetch(
            'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
            {
              method: 'GET',
              headers: {
                Accept: 'application/json',
              },
            },
          );
    
          if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
          }
    
          const result = await response.json();
    
          return result.hits;
        } catch (error) {
            throw error;
        }
      }
    
      /**
       * Method that loads the news to the database
       * @returns A void Promise
       */
      async loadNewsToDatabase() {

        try{
            const news = this.getNewsFromAPI();
        
            await this.newsModel.deleteMany();
        
            news
              .then((items) => {
                items.forEach((item) => {
                  const createNews = new News(
                    item['title'],
                    item['created_at'],
                    item['url'],
                    item['author'],
                    item['points'],
                    item['story_text'],
                    item['comment_text'],
                    item['num_comments'],
                    item['story_id'],
                    item['story_title'],
                    item['story_url'],
                    item['parent_id'],
                    item['created_at_i'],
                    item['_tags'],
                  );
        
                  const news = new this.newsModel(createNews);
                  news.save();
                });
        
                console.log('Database updated at:', new Date());
              })
              .catch((error) => {
                throw error;
              });

        }
        catch (error) {
            console.log(`Database couldn't be updated: ${error}`);
            throw error;
        }
      }
    
      /** Method to repopulate database once an hour from external API */
      @Cron(CronExpression.EVERY_HOUR)
      async actualizeDatabaseDataFromAPIEveryHour() {
        this.loadNewsToDatabase();
      }
}
