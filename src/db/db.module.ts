import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsSchema } from 'src/schemas/news.schema';
import { DbController } from './db.controller';
import { DbService } from './db.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'News',
        schema: NewsSchema
      }
    ]),
  ],
  controllers: [DbController],
  providers: [DbService]
})
export class DbModule {}
