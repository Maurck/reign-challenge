import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Query,
  UseGuards,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { GetNewsByAuthorQueryDto } from '../dto/getNewsByAuthorQueryDto.dto';
import { GetNewsByTagQueryDto } from '../dto/getNewsByTagQueryDto.dto';
import { NewsService } from './news.service';
import { News } from '../classes/news.class';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { GetNewsByTitleQueryDto } from '../dto/getNewsByTitleQueryDto.dto';
import { GetNewsByMonthQueryDto } from '../dto/getNewsByMonthQueryDto.dto';
import { DeleteNewsByIdQueryDto } from '../dto/deleteNewsByIdQueryDto.dto';


/** News Controller */
@ApiTags('news')
@Controller('news')
export class NewsController {

  /** @ignore */
  constructor(private newsService: NewsService) {}

  /**
   * GET Method to get news by author
   * @param res Express response object
   * @param query Query data transfer object
   * @returns Promise that contains a response of type INews
   */
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Sucessful news by author name search',
    type: News,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad request'
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not get news by author name',
  })
  @ApiOperation({ summary: 'Search news by author' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Get('/author')
  async getByAuthor(
    @Res() res: Response,
    @Query() query: GetNewsByAuthorQueryDto,
  ) {
    try {
      const news = await this.newsService.getNewsByAuthor(query);
      return res.status(HttpStatus.OK).json(news);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        msg: 'Could not get news by author name',
      });
    }
  }

  /**
   * GET Method to get news by tag
   * @param res Express response object
   * @param query Query data transfer object
   * @returns Promise that contains a response of type INews
   */
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Succesful news by tag search',
    type: News,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad request'
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not get news by tag',
  })
  @ApiOperation({ summary: 'Search news by tag' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Get('/tag')
  async getByTag(@Res() res: Response, @Query() query: GetNewsByTagQueryDto) {
    try {
      const news = await this.newsService.getNewsByTag(query);
      return res.status(HttpStatus.OK).json(news);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        msg: 'Could not get news by tag',
      });
    }
  }

  /**
   * GET Method to get news by title
   * @param res Express response object
   * @param query Query data transfer object
   * @returns Promise that contains a response of type INews
   */
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Succesful news by title search',
    type: News,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad request'
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not get news by title',
  })
  @ApiOperation({ summary: 'Search news by title' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Get('/title')
  async getByTitle(
    @Res() res: Response,
    @Query() query: GetNewsByTitleQueryDto,
  ) {
    try {
      const news = await this.newsService.getNewsByTitle(query);
      return res.status(HttpStatus.OK).json(news);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        msg: 'Could not get news by title',
      });
    }
  }

  /**
   * GET Method to get news by month
   * @param res Express response object
   * @param query Query data transfer object
   * @returns Promise that contains a response of type INews
   */
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Succesful news by month search',
    type: News,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad request'
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not get news by month',
  })
  @ApiOperation({ summary: 'Search news by month' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Get('/month')
  async getByMonth(
    @Res() res: Response,
    @Query() query: GetNewsByMonthQueryDto,
  ) {
    try {
      const news = await this.newsService.getNewsByMonth(query);
      return res.status(HttpStatus.OK).json(news);
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        msg: 'Could not get news by month',
      });
    }
  }

  /**
   * DELETE Method to delete news by id
   * @param res Express response object
   * @param query Query data transfer object
   * @returns Promise that contains a response of type INews
   */
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Succesful delete news by id'
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad request'
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Could not delete news by id',
  })
  @ApiOperation({ summary: 'Delete news by mongoDB id' })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @Delete()
  async deleteById(
    @Res() res: Response,
    @Query() query: DeleteNewsByIdQueryDto,
  ): Promise<Response> {
    try {
      const newsDelete = await this.newsService.deleteNewsById(query);
      if (!newsDelete) throw new NotFoundException();
      return res.status(HttpStatus.OK).json({
        msg: 'News deleted by id',
      });
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        error: 'Could not delete news by id',
      });
    }
  }
}
