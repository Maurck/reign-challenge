import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { Response } from 'express';

describe('NewsController', () => {
  let controller: NewsController;

  const mockNewsService = {
    getNewsByAuthor: jest.fn( data => {
			return {
        _id: 1,
        ...data
      }
		})
  };

  let mockResponse: Partial<Response>;

  let resultJson = {};
  let resultStatus = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(mockNewsService)
      .compile();

    controller = module.get<NewsController>(NewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should get news by author', async () => {
    mockResponse = {};

    resultJson = {};
    resultStatus = {};

    mockResponse.status = jest.fn().mockImplementation((result) => {
      resultStatus = result;
      return mockResponse;
    });
    mockResponse.json = jest.fn().mockImplementation((result) => {
      resultJson = result;
      return mockResponse;
    });

    await controller.getByAuthor(mockResponse as Response, {
      author: 'Mauricio',
    })

    expect(resultJson).toEqual({
      _id: expect.any(Number),
      author: 'Mauricio'
    });
  });
});
