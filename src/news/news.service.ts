import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { INews } from '../interface/news.interface';
import { GetNewsByAuthorQueryDto } from '../dto/getNewsByAuthorQueryDto.dto';
import { GetNewsByTagQueryDto } from 'src/dto/getNewsByTagQueryDto.dto';
import { GetNewsByTitleQueryDto } from 'src/dto/getNewsByTitleQueryDto.dto';
import { GetNewsByMonthQueryDto } from 'src/dto/getNewsByMonthQueryDto.dto';
import { DeleteNewsByIdQueryDto } from 'src/dto/deleteNewsByIdQueryDto.dto';


/** News Service */
@Injectable()
export class NewsService {

  /** @ignore */
  constructor(@InjectModel('News') private readonly newsModel: Model<INews>) {}

  /**
   * Get News by Author
   * @param query Data Transfer Object that represents the query
   * @returns Promise of News
   */
  async getNewsByAuthor(query: GetNewsByAuthorQueryDto): Promise<INews[]> {
    const { author, from = 0, limit = 5 } = query;

    try {
      const escapedAuthor = author.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
      const authorRegex = new RegExp(escapedAuthor, 'i');

      const news = await this.newsModel
        .find({ author: { $regex: authorRegex } })
        .skip(from)
        .limit(limit)
        .exec();

      return news;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  /**
   * Get News by Tag
   * @param query Data Transfer Object that represents the query
   * @returns Promise of News array
   */
  async getNewsByTag(query: GetNewsByTagQueryDto): Promise<INews[]> {
    const { tag, from = 0, limit = 5 } = query;

    try {
      const escapedTag = tag.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
      const tagRegex = new RegExp(escapedTag, 'i');

      const news = await this.newsModel
        .find({
          tags: { $regex: tagRegex },
        })
        .skip(from)
        .limit(limit)
        .exec();

      return news;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  /**
   * Get News by Title
   * @param query Data Transfer Object that represents the query
   * @returns Promise of News array
   */
  async getNewsByTitle(query: GetNewsByTitleQueryDto): Promise<INews[]> {
    const { title, from = 0, limit = 5 } = query;

    try {
      const escapedTitle = title.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
      const tagRegex = new RegExp(escapedTitle, 'i');

      const news = await this.newsModel
        .find({
          title: { $regex: tagRegex },
        })
        .skip(from)
        .limit(limit)
        .exec();

      return news;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  /**
   * Get News by Month
   * @param query Data Transfer Object that represents the query
   * @returns Promise of News array
   */
  async getNewsByMonth(query: GetNewsByMonthQueryDto): Promise<INews[]> {
    const { month, from = 0, limit = 5 } = query;

    const months = {
      january: '01',
      february: '02',
      march: '03',
      april: '04',
      may: '05',
      june: '06',
      july: '07',
      august: '08',
      september: '09',
      october: '10',
      november: '11',
      december: '12',
    };

    try {
      const startString = `2022-${months[month]}-01`;
      const start = new Date(startString);
      start.setHours(0, 0, 0, 0);

      const endString = `2022-${months[month]}-31`;
      const end = new Date(endString);
      end.setHours(23, 59, 59, 999);

      const news = await this.newsModel
        .find({
          createdAt: {
            $gte: start,
            $lte: end,
          },
        })
        .skip(from)
        .limit(limit)
        .exec();

      return news;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  /**
   * Delete News by Id
   * @param query Data Transfer Object that represents the query
   * @returns Promise of News array
   */
  async deleteNewsById(query: DeleteNewsByIdQueryDto): Promise<INews> {
    const { id } = query;

    try {
      const deletedNews = await this.newsModel.findOneAndDelete({ _id: id });
      return deletedNews;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
