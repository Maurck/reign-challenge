import { Injectable } from '@nestjs/common';

/** Users service */
@Injectable()
export class UsersService {

  /** Method to validate user by its username */
  validate(username: string): boolean {
    return username == process.env.ADMIN_USERNAME;
  }
}