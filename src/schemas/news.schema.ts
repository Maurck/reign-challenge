import { Schema } from "mongoose";

/** News mongoose schema */
export const NewsSchema = new Schema({
    title: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String
    },
    author: {
        type: String
    },
    points: {
        type: String
    },
    storyText: {
        type: String
    },
    commentText: {
        type: String
    },
    numComments: {
        type: String
    },
    storyId: {
        type: Number
    },
    storyTitle: {
        type: String
    },
    storyUrl: {
        type: String
    },
    parentId: {
        type: Number
    },
    createdAtI: {
        type: Number
    },
    tags: {
        type: [String]
    }
})