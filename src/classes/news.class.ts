import { ApiProperty } from "@nestjs/swagger"

/** Class that represents news typescript object abstraction to interact with Mongoose as a Model */
export class News {
    
    /** Title of the news */
    @ApiProperty()
    title: string

    /** Date of creation of the news */
    @ApiProperty()
    createdAt: Date

    /** Source URL of the news */
    @ApiProperty()
    url: string

    /** Author nickname of the news */
    @ApiProperty()
    author: string

    /** Points property */
    @ApiProperty()
    points: string

    /** Story text of the news */
    @ApiProperty()
    storyText: string

    /** Text comment of the news */
    @ApiProperty()
    commentText: string

    /** numComments property */
    @ApiProperty()
    numComments: string

    /** Story ID of the news */
    @ApiProperty()
    storyId: number

    /** Story title of the news */
    @ApiProperty()
    storyTitle: string

    /** Story URL of the news */
    @ApiProperty()
    storyUrl: string

    /** parentId property */
    @ApiProperty()
    parentId: number

    /** createdAtI property */
    @ApiProperty()
    createdAtI: number

    /** tags of the news */
    @ApiProperty()
    tags: string[]

    /** @ignore */
    constructor(title: string, createdAt: Date, url: string, author: string, points: string, storyText: string, commentText: string, numComments: string, storyId: number, storyTitle: string, storyUrl: string, parentId: number, createdAtI: number, tags: [string]) {
        this.title = title;
        this.createdAt = createdAt;
        this.url = url;
        this.author = author;
        this.points = points;
        this.storyText = storyText;
        this.commentText = commentText;
        this.numComments = numComments;
        this.storyId = storyId;
        this.storyTitle = storyTitle;
        this.storyUrl = storyUrl;
        this.parentId = parentId;
        this.createdAtI = createdAtI;
        this.tags = tags;
    }
}