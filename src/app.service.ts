import { Injectable } from '@nestjs/common';

/** Application Service */
@Injectable()
export class AppService {

  /**
   * Method that returns a greetings string
   * @returns Hello string
   */
  helloReign(): string {
    return 'Hola desarrolladores de Reign :)';
  }
}
