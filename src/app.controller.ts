import { Controller, Get} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

/** Application Controller */
@ApiTags('home')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * GET Method to display 'Hello Reign'
   * @returns Hello String
   */
  @Get()
  helloReign(): string {
    return this.appService.helloReign();
  }
}