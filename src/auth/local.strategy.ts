import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

/** Local strategy that extends from PassportStrategy to handle local authentication */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {

  /** @ignore */
  constructor(private authService: AuthService) {
    super();
  }

  /**
   * Method to validate the user for JWT
   * @param username Username to validate
   * @param password Password to validate
  */
  validate(username: string, password: string): string {
    const validatedUserName = this.authService.validateUser(username, password);
    if (!validatedUserName) {
      throw new UnauthorizedException();
    }
    return validatedUserName;
  }
}