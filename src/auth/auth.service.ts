import { Inject, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

/** Authentication Service */
@Injectable()
export class AuthService {
  
  /** JWT Service */
  @Inject(JwtService) 
  private jwtService: JwtService

  /** @ignore */
  constructor(
    private usersService: UsersService
  ) {}

  /**
   *  Method to validate the user
   * @param username Username to verify
   * @param pass Password to verify
   * @returns {Promise} Promise containing the JWT
   */
  validateUser(username: string, pass: string): string {
    const userFound = this.usersService.validate(username);
    if (userFound && pass == process.env.ADMIN_PASSWORD) {
      return username;
    }
    return null;
  }

  /**
   * Returns a JWT using a username
   * @param username Username to sign JWT
   * @returns 
   */
  async getJWTToken(username: string): Promise<any> {
    const payload = { username };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}