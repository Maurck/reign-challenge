import { Controller, HttpStatus, Query, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginQueryDto } from '../dto/loginQueryDto.dto';

/**
 * Authentication Controller
 * @returns Promise containing JWT token
 */
@ApiTags('auth')
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {}

    /**
     * GET Method to get JWT Token
     * @param loginBodyDto Body Dto for the login query
     */
    @ApiResponse(
      { 
          status: HttpStatus.OK, 
          description: 'Get Json Web Token'
      }
    )
    @ApiResponse(
      { 
          status: HttpStatus.UNAUTHORIZED, 
          description: 'Username or password incorrect'
      }
    )
    @ApiOperation({ summary: 'Get JWT' })
    @Get('login')
    async login(@Query() loginBodyDto: LoginQueryDto): Promise<any> {
      return this.authService.getJWTToken(loginBodyDto.username);
    }
}
