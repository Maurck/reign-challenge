import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 * Guard that verifies that a JWT is present in header and is valid
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {}
