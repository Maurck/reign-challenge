import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

/**
 * JWT strategy that extends from PasssportStrategy to handle JWT authentication
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

  /** @ignore */
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: "secretkey",
    });
  }

  /** 
   * Method to validate payload 
   * @Param payload of the JWT
  */
  async validate(payload: any) {
    return { username: payload.username };
  }
}