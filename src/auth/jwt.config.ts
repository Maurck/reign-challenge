import { JwtModuleAsyncOptions } from "@nestjs/jwt";
import appConfig from './auth.config'

/**
 * Asyc module to configure JWT
 */
export const  jwtConfig: JwtModuleAsyncOptions = {
    useFactory: () => {
        return {
            secret: appConfig().secretKey,
            signOptions: { expiresIn: "24h"},
        }
    }
}