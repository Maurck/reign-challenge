import { Document } from "mongoose";

/** Interface that represents news for API response and Swagger documentation. */
export interface INews extends Document{

    /** News ID in database */
    readonly id?: number

    /** News title */
    readonly title: string

    /** News creation */
    readonly createdAt: Date

    /** News url */
    readonly url: string

    /** News author name */
    readonly author: string

    /** News point parameter */
    readonly points: string

    /** News storyText parameter */
    readonly storyText: string

    /** News commentText parameter */
    readonly commentText: string

    /** News numCommets parameter */
    readonly numComments: string

    /** News storyId parameter */
    readonly storyId: number

    /** News storyTitle parameter */
    readonly storyTitle: string

    /** News storyUrl parameter */
    readonly storyUrl: string

    /** News parentId parameter */
    readonly parentId: number

    /** News createdAtI parameter */
    readonly createdAtI: number

    /** News tags */
    readonly tags: string[]
}