import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { GetNewsQueryDto } from './getNewsQueryDto.dto';

/** Class that represents a Data Transfer Object for the Get News bt tag Query */
export class GetNewsByTagQueryDto extends GetNewsQueryDto{
 
    /** Tag name */
    @ApiProperty({
        description: 'Tag name'
    })
    @IsString()
    tag: string
}