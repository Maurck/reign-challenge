import { ApiProperty } from '@nestjs/swagger';
import { GetNewsQueryDto } from "./getNewsQueryDto.dto";
import { IsIn, IsString } from 'class-validator';

/** Class that represents a Data Transfer Object for the Get News by month Query */
export class GetNewsByMonthQueryDto extends GetNewsQueryDto {
 
    /** Month to search */
    @ApiProperty({
        description: 'Month to search'
    })
    @IsString()
    @IsIn(['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'])
    month: string
}