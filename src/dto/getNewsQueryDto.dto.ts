import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsOptional, Min } from 'class-validator';

/** Class that represents a Data Transfer Object for the Get News bt tag Query */
export class GetNewsQueryDto {

  /** Number of items to skip in query */
  @ApiProperty({
    required: false,
    description: 'Number of items to skip in query',
  })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Type(() => Number)
  from?: number;

  /** Limit of items to return in query */
  @ApiProperty({
    required: false,
    description: 'Limit of items to return in query'
  })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Type(() => Number)
  limit?: number;
}
