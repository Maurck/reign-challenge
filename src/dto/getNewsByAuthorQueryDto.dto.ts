import { ApiProperty } from '@nestjs/swagger';
import { GetNewsQueryDto } from "./getNewsQueryDto.dto";
import { IsString } from 'class-validator';

/** Class that represents a Data Transfer Object for the Get News by author Query */
export class GetNewsByAuthorQueryDto extends GetNewsQueryDto {
 
    /** Author name */
    @ApiProperty({
        description: 'Author name'
    })
    @IsString()
    author: string
}