import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/** Class that represents a Data Transfer Object for the Login Query */
export class LoginQueryDto {

    /** Name of the user */
    @ApiProperty({
        description: 'Name of user'
    })
    @IsString()
    username: string

    /** Password of the user */
    @ApiProperty({
        description: 'Password of user'
    })
    @IsString()
    password: string
}