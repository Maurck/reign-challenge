import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';

/** Class that represents a Data Transfer Object for the Delete Query */
export class DeleteNewsByIdQueryDto  {
 
    /** Id of news to delete */
    @ApiProperty({
        description: 'Id of news to delete'
    })
    @MinLength(24)
    @MaxLength(24)
    @IsString()
    id: string
}