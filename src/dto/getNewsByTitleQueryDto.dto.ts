import { ApiProperty } from '@nestjs/swagger';
import { GetNewsQueryDto } from "./getNewsQueryDto.dto";
import { IsString } from 'class-validator';

/** Class that represents a Data Transfer Object for the Get News bt title Query */
export class GetNewsByTitleQueryDto extends GetNewsQueryDto {
 
    /** Title of news */
    @ApiProperty({
        description: 'Title of news'
    })
    @IsString()
    title: string
}